// The MIT License (MIT)
//
// Copyright (c) 2021 Leon Manukyan
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef YEREVAN_STRATOSPHERE_H
#define YEREVAN_STRATOSPHERE_H

#ifdef __cplusplus
extern "C" {
#endif

typedef enum
{
    ASCENDING = 0,
    BAILEDOUT = 1,
} HAPState;

typedef unsigned long Milliseconds;

typedef struct Acceleration
{
    double ax, ay, az;
} Acceleration;

// Duration (in milliseconds) of free-fall triggering a bail-out event
// The average value of the accelerometer value over that time span
// must be under 0.5G.
#define HAP_FREE_FALL_TIME_WINDOW 1024

#define HAP_MAX_FLIGHT_TIME_MINUTES 120

#define HAP_ACCEL_BUF_SIZE 8

typedef struct HAP
{
    Milliseconds start_time;
    Milliseconds last_update_time;
    HAPState state;
    double a2[HAP_ACCEL_BUF_SIZE];
    double a2sum;
    int curr_slot_index;
    Milliseconds curr_slot_t;
    double curr_slot_a2sum;
} HAP;

void hap_init(HAP* hap, Milliseconds time);
HAPState hap_update(HAP* hap, Milliseconds time, Acceleration a);

#ifdef __cplusplus
}
#endif

#endif // YEREVAN_STRATOSPHERE_H
