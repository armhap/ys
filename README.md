# Prerequisites

- meson
- ninja
- googletest

Under ubuntu 20.04 install those with

```
apt install meson ninja-build libgtest-dev
```

# Building/testing

```
cd <repo_directory>
meson build .
# Above command should be run only once in a clean repository

cd build

ninja && ./unittests
# or
meson test -v
```

The script `cbuild_and_test` starts a continuous build and test loop that recompiles
and runs the unit tests on every modification of the sources. 
