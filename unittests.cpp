// The MIT License (MIT)
//
// Copyright (c) 2021 Leon Manukyan
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "ys.h"

#include <gtest/gtest.h>

const double G = 9.8;

struct SimulationPoint {
    Milliseconds t;
    Acceleration a;
    HAPState s;
};

#define CHECK_HAP_UPDATE(t, a, s) \
        ASSERT_EQ(s, hap_update(&hap, t, a)) << "t=" << (t)

void ASSERT_SIMULATION(const std::vector<SimulationPoint>& sim)
{
    HAP hap;
    hap_init(&hap, sim[0].t);
    ASSERT_EQ(ASCENDING, sim[0].s);
    for ( size_t i = 1; i < sim.size(); ++i ) {
        CHECK_HAP_UPDATE(sim[i].t, sim[i].a, sim[i].s);
    }
}

TEST(YS, shouldNotBailoutWhileInAStateOfRest)
{
    ASSERT_SIMULATION({
        //  time,     accel, state
        {      0, {0, 0, G}, ASCENDING},
        { 100000, {0, 0, G}, ASCENDING},
    });
}

TEST(YS, shouldBailoutIfMaxFlightTimeIsExceeded)
{
    ASSERT_SIMULATION({
        //   time,     accel, state
        {       0, {0, 0, G}, ASCENDING},
        { 1000000, {0, 0, G}, ASCENDING},
        { 2000000, {0, 0, G}, ASCENDING},
        { 3000000, {0, 0, G}, ASCENDING},
        { 4000000, {0, 0, G}, ASCENDING},
        { 5000000, {0, 0, G}, ASCENDING},
        { 6000000, {0, 0, G}, ASCENDING},
        { 7000000, {0, 0, G}, ASCENDING},
        { 8000000, {0, 0, G}, BAILEDOUT},
    });
}

TEST(YS, shouldBailoutUponDetectingFreeFall)
{
    ASSERT_SIMULATION({
        //  time,             accel, state
        {      0, { 0.0, 0.0, 9.81}, ASCENDING},
        {  20000, { 0.0, 0.0, 9.81}, ASCENDING},
        {  40000, { 0.0, 0.0, 9.81}, ASCENDING},
        {  60000, { 0.0, 0.0, 9.81}, ASCENDING},
        {  60100, { 0.0, 0.0, 0.23}, ASCENDING},
        {  60200, { 0.0, 0.0, 0.48}, ASCENDING},
        {  60300, { 0.0, 0.0, 0.82}, ASCENDING},
        {  60400, { 0.0, 0.0, 1.30}, ASCENDING},
        {  60500, { 0.0, 0.0, 1.97}, ASCENDING},
        {  60600, { 0.0, 0.0, 2.56}, ASCENDING},
        {  60700, { 0.0, 0.0, 3.70}, ASCENDING},
        {  60800, { 0.0, 0.0, 4.84}, ASCENDING},
        {  60900, { 0.0, 0.0, 6.30}, ASCENDING},
        {  61000, { 0.0, 0.0, 8.03}, BAILEDOUT},
    });
}

TEST(YS, shouldNotBailoutBecauseOfIntermittentFreeFallAnomalies)
{
    ASSERT_SIMULATION({
        //  time,             accel, state
        {      0, { 0.0, 0.0, 9.81}, ASCENDING},
        {  20000, { 0.0, 0.0, 9.81}, ASCENDING},
        {  40000, { 0.0, 0.0, 9.81}, ASCENDING},
        {  60000, { 0.0, 0.0, 9.81}, ASCENDING},
        {  60100, { 0.0, 0.0, 0.23}, ASCENDING},
        {  60200, { 0.0, 0.0, 0.48}, ASCENDING},
        {  60300, { 0.0, 0.0, 7.56}, ASCENDING},
        {  60400, { 0.0, 0.0, 8.30}, ASCENDING},
        {  60500, { 0.0, 0.0, 9.78}, ASCENDING},
        {  60600, { 0.0, 0.0, 9.81}, ASCENDING},
        {  60700, { 0.0, 0.0, 9.81}, ASCENDING},
        {  60800, { 0.0, 0.0, 9.81}, ASCENDING},
        {  60900, { 0.0, 0.0, 9.81}, ASCENDING},
        {  61000, { 0.0, 0.0, 9.81}, ASCENDING},
    });
}
