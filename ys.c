// The MIT License (MIT)
//
// Copyright (c) 2021 Leon Manukyan
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "ys.h"

const Milliseconds MAX_FLIGHT_TIME_MILLIS = HAP_MAX_FLIGHT_TIME_MINUTES * 60000;

const Milliseconds ACCEL_TIME_SLICE = HAP_FREE_FALL_TIME_WINDOW / HAP_ACCEL_BUF_SIZE;


const double G = 9.81;
const double G2 = G*G;

Milliseconds mymin(Milliseconds a, Milliseconds b)
{
    return a < b ? a : b;
}

double sq(double x)
{
    return x*x;
}

double accel_squared(Acceleration a)
{
    return sq(a.ax)+sq(a.ay)+sq(a.az);
}


void hap_init(HAP* hap, Milliseconds time)
{
    hap->start_time = time;
    hap->last_update_time = time;
    hap->state = ASCENDING;
    for(int i = 0; i < HAP_ACCEL_BUF_SIZE; ++i )
        hap->a2[i] = G2*ACCEL_TIME_SLICE;
    hap->a2sum = G2*HAP_FREE_FALL_TIME_WINDOW;
    hap->curr_slot_t = 0;
    hap->curr_slot_index = 0;
    hap->curr_slot_a2sum = 0;
}

HAPState hap_update(HAP* hap, Milliseconds time, Acceleration accel)
{
    if ( time - hap->start_time > MAX_FLIGHT_TIME_MILLIS )
        hap->state = BAILEDOUT;

    Milliseconds dt = time - hap->last_update_time;
    const double a2 = accel_squared(accel);
    while ( dt > 0 ) {
        const Milliseconds dt1 = mymin(dt, ACCEL_TIME_SLICE - hap->curr_slot_t);
        hap->curr_slot_t += dt1;
        hap->curr_slot_a2sum += dt1 * a2;
        if ( hap->curr_slot_t == ACCEL_TIME_SLICE ) {
            hap->a2sum -= hap->a2[hap->curr_slot_index];
            hap->a2sum += hap->curr_slot_a2sum;
            hap->a2[hap->curr_slot_index++] = hap->curr_slot_a2sum;
            if ( hap->curr_slot_index == HAP_ACCEL_BUF_SIZE )
                hap->curr_slot_index = 0;
            hap->curr_slot_t = 0;
            hap->curr_slot_a2sum = 0;
        }
        dt -= dt1;
    }
    hap->last_update_time = time;

    if ( hap->a2sum < HAP_FREE_FALL_TIME_WINDOW * G2 / 4 )
        hap->state = BAILEDOUT;

    return hap->state;
}
